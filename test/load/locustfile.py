# ---------------------------------
# basic load test of html app
# ---------------------------------

from locust import HttpLocust, TaskSet, task
import json
# install: `pip install locustio`
# start server: locust -f locustfile.py --host=http://ec2-34-201-154-8.compute-1.amazonaws.com:31040/
# run/monitor test: http://localhost:8089/


class WebsiteTasksBasic(TaskSet):

    # index
    @task
    def get_index(self):
        self.client.get('/')


class WebsiteUser(HttpLocust):
    task_set = WebsiteTasksBasic
    # notice: specify the --host flag to locust if you want to override.
    # direct
    host = "http://ec2-34-201-154-8.compute-1.amazonaws.com:31040/"
    #print('Running with host: {0}'.format(host))
    min_wait = 5 * 1000    # 5s: min wait time between execution of tasks
    max_wait = 15 * 1000   # 15s: max wait time between execution of tasks
